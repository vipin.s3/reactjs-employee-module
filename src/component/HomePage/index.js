import React from 'react'
import Form from './DetailsForm'
import { fetchHome } from './ActionCreators'
import { connect } from 'react-redux'

const Home = (props) => {
  console.log('homeReduxData', props.homeReduxData)
  // props destructuring
  // state definition
  // funcfetchHometions (arrow)
  // default api calls using useEffect

  return (
    <div className="text-center">
      <Form />
    </div>
  )
}
//useSelector
const mapStateToProps = (state) => {
  return {
    homeReduxData: state.home
  }
}
//useDispatcher
const mapDispatchToProps = (dispatch) => {
  return {
    fetchHome: (state) => dispatch(fetchHome(state))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home)
