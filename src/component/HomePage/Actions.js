import { REQUEST_HOME, RECEIVE_HOME, FAILURE_HOME } from 'ActionTypes'

/**
 * @description Request action for getting home page data
 */
export const requestHome = () => ({
  type: REQUEST_HOME
})

/**
 * @description Receive action for getting home page data
 */
export const receiveHome = (payload) => ({
  type: RECEIVE_HOME,
  payload
})

/**
 * @description Failure action for fetching home page data
 */
export const failureHome = (payload) => ({
  type: FAILURE_HOME,
  payload
})
