import { REQUEST_HOME, RECEIVE_HOME, FAILURE_HOME } from 'ActionTypes'

const initialState = {
  isHomeLoading: false,
  isError: false,
  homeErrorMessage: ''
}
const Home = (state = initialState, action) => {
  const { type, payload } = action
  switch (type) {
    case REQUEST_HOME:
      return {
        ...state,
        isLoading: true
      }
    case RECEIVE_HOME:
      return {
        ...state,
        isHomeLoading: false,
        isError: false,
        home: payload?.data
      }
    case FAILURE_HOME:
      return {
        ...state,
        isHomeLoading: false,
        isError: true,
        homeErrorMessage: payload?.errorMessage
      }
    default:
      return {
        ...state
      }
  }
}
export default Home
