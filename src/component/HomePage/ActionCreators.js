import axios from 'axios'
import { HOME_PAGE_DATA_API, DEFAULT_ERROR_MESSAGE } from 'common/constants'
import { requestHome, receiveHome, failureHome } from './Actions'

/**
 * @description Fetch Dummy API on Home page
 * @param {} dispatch
 */
export function fetchHome({ apidata }) {
  return (dispatch) => {
    dispatch(requestHome())
    return axios
      .post(
        HOME_PAGE_DATA_API,
        {
          ...apidata
        },
        {
          headers: {
            'Program-Id': 'dummy'
          }
        }
      )
      .then((response) => {
        dispatch(receiveHome(response.data))
      })
      .catch((err) => {
        dispatch(failureHome(err.response?.data || DEFAULT_ERROR_MESSAGE))
      })
  }
}
