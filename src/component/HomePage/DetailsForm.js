import React, { useState, useEffect } from 'react'
import DataTable from './DetailsTable'
import { connect } from 'react-redux'
import { fetchHome } from './ActionCreators'
import { useHistory } from 'react-router-dom'

const DeatilsForm = () => {
  const history = useHistory()
  const [first, setFirst] = useState('')
  const [last, setLast] = useState('')
  const [dob, setDob] = useState('')
  const [designation, setDesignation] = useState('')
  const [experience, setExperience] = useState('')
  const [img, setImg] = useState('')
  const [dataWorking, setDataWorking] = useState([])
  const [defaultData, setDefaultData] = useState(false)

  useEffect(() => {
    if (defaultData) {
      setFirst('FirstName')
      setLast('LasttName')
      setDob('2021-08-11')
      setDesignation('Sr. Dev')
      setExperience('12')
      setImg('https://picsum.photos/200/300')
    }
  }, [defaultData])

  const handleEdit = (obj) => {
    setFirst(obj.first)
    setLast(obj.last)
    setDob(obj.dob)
    setDesignation(obj.designation)
    setExperience(obj.experience)
    setImg(obj.img)
  }
  const handleClear = () => {
    setFirst('')
    setLast('')
    setDob('')
    setDesignation('')
    setExperience('')
    setImg('')
  }
  const handleDelete = (text = '') => {
    const key = text
    const newData = dataWorking.filter((temp) => {
      return temp.key !== key
    })
    console.log('newData', newData, dataWorking, key)
    setDataWorking([...newData])
  }
  const handleChange = (e) => {
    const { name, value } = e.target
    switch (name) {
      case 'firstName':
        setFirst(value)
        break
      case 'lastName':
        setLast(value)
        break
      case 'dob':
        setDob(value)
        break
      case 'designation':
        setDesignation(value)
        break
      case 'Experience':
        setExperience(value)
        break
      case 'image':
        setImg(value)
        break
      default:
        return
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    const obj = {
      first,
      last,
      dob,
      designation,
      experience,
      img
    }
    obj.key = first + last + dob + (dataWorking.length - 1)
    console.log('datatep', ...dataWorking, obj)
    setDataWorking([...dataWorking, obj])
    handleClear()
  }
  const handleLogin = () => {
    history.push('/login')
  }
  return (
    <div>
      <div style={{ display: 'flex-inline' }}>
        <button onClick={() => handleLogin()}>Login</button>
      </div>
      <h3 className="text-center">Employee Form</h3>
      <button style={{ margin: '3px' }} onClick={() => setDefaultData(true)}>
        Click me to Fill default data!
      </button>
      <div style={{ display: 'flex-inline' }}>
        <input
          type="number"
          value={first}
          style={{ margin: '3px' }}
          onChange={handleChange}
          name="firstName"
          id="firstName"
          placeholder="Enter First name"
        />
        <input
          type="text"
          value={last}
          style={{ margin: '3px' }}
          onChange={handleChange}
          name="lastName"
          id="lastName"
          placeholder="Enter Last name"
        />
        <input
          type="date"
          name="dob"
          id="dob"
          value={dob}
          style={{ margin: '3px' }}
          onChange={handleChange}
          placeholder="Enter Date of Birth"
        />
        <input
          type="text"
          value={designation}
          style={{ margin: '3px' }}
          onChange={handleChange}
          name="designation"
          id="designation"
          placeholder="Enter designation"
        />
        <input
          value={experience}
          style={{ margin: '3px' }}
          onChange={handleChange}
          type="number"
          id="experience"
          name="Experience"
          placeholder="Experience in months"
        />
        <input
          type="media"
          value={img}
          style={{ margin: '3px' }}
          onChange={handleChange}
          name="image"
          id="image"
          placeholder="Provide your profile image link"
        />
        <button onClick={handleClear}>Clear Form</button>
        <button onClick={handleSubmit}>Add Details</button>
      </div>
      <div>
        <h4 className="text-center">Employee Data</h4>
        <DataTable
          dataWorking={dataWorking}
          handleEdit={handleEdit}
          handleDelete={handleDelete}
        />
      </div>
    </div>
  )
}

//useSelector
const mapStateToProps = (state) => {
  return {
    reduxData: state.temp
  }
}
//useDispatcher
const mapDispatchToProps = (dispatch) => {
  return {
    fetchHome: (state) => dispatch(fetchHome(state))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(DeatilsForm)
