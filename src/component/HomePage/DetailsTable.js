import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { fetchHome } from './ActionCreators'

// store

const DetailsTable = (props) => {
  // const { fetchHome, reduxData } = props
  const [data, setdata] = useState([])
  const { handleEdit, handleDelete, dataWorking } = props
  console.log('data', data, dataWorking)
  useEffect(() => {
    dataWorking.length && setdata(dataWorking)
  }, [dataWorking])
  return (
    <div>
      {!dataWorking?.length ? (
        'No Data to display'
      ) : (
        <>
          <div style={{ display: 'flex' }}>
            <div style={{ width: '100px', fontWeight: '600', padding: '5px' }}>
              First Name
            </div>
            <div style={{ width: '100px', fontWeight: '600', padding: '5px' }}>
              Last Name
            </div>
            <div style={{ width: '100px', fontWeight: '600', padding: '5px' }}>
              Designation
            </div>
            <div style={{ width: '100px', fontWeight: '600', padding: '5px' }}>
              Date of Birth
            </div>
            <div style={{ width: '100px', fontWeight: '600', padding: '5px' }}>
              Experience
            </div>
            <div style={{ width: '100px', fontWeight: '600', padding: '5px' }}>
              Profile Pic
            </div>
          </div>
          <div>
            {dataWorking.map((temp, i) => {
              return (
                <div style={{ display: 'flex' }} key={i + temp.key}>
                  <div style={{ width: '100px', padding: '5px' }}>
                    {temp.first}
                  </div>
                  <div style={{ width: '100px', padding: '5px' }}>
                    {temp.last}
                  </div>
                  <div style={{ width: '100px', padding: '5px' }}>
                    {temp.designation}
                  </div>
                  <div style={{ width: '100px', padding: '5px' }}>
                    {temp.dob}
                  </div>
                  <div style={{ width: '100px', padding: '5px' }}>
                    {temp.experience}
                  </div>
                  <div style={{ width: '100px', height: '100px' }}>
                    <img
                      src={temp.img}
                      style={{ width: '100px', height: '100px' }}
                      alt="ProfileImg"
                    />{' '}
                  </div>
                  <div style={{ width: '100px', padding: '5px' }}> </div>
                  <div style={{ width: '20px', padding: '5px' }}>
                    <button
                      style={{ width: '100px', marginBottom: '10px' }}
                      onClick={() => handleDelete(temp.key)}
                    >
                      Delete
                    </button>
                    <button
                      style={{ width: '100px' }}
                      onClick={() => handleEdit(temp)}
                    >
                      Edit
                    </button>
                  </div>
                </div>
              )
            })}
          </div>
        </>
      )}{' '}
    </div>
  )
}
//useSelector
const mapStateToProps = (state) => {
  return {
    reduxData: state
  }
}
//useDispatcher
const mapDispatchToProps = (dispatch) => {
  return {
    fetchHome: (state) => dispatch(fetchHome(state))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(DetailsTable)
