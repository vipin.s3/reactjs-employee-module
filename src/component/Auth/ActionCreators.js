import axios from 'axios'
import { LOGIN_API, DEFAULT_ERROR_MESSAGE } from 'common/constants'
import { requestOTP, receiveOTP, failureOTP } from './Actions'

/**
 * @description Fetch Dummy API on OTP page
 * @param {} dispatch
 */
export function fetchOTP({ apidata }) {
  return (dispatch) => {
    dispatch(requestOTP())
    return axios
      .post(
        LOGIN_API,
        {
          ...apidata
        },
        {
          headers: {
            'Program-Id': 'dummy'
          }
        }
      )
      .then((response) => {
        dispatch(receiveOTP(response.data))
      })
      .catch((err) => {
        dispatch(failureOTP(err.response?.data || DEFAULT_ERROR_MESSAGE))
      })
  }
}
