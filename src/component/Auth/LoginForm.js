import React, { useState } from 'react'
import { connect } from 'react-redux'
import { fetchOTP } from './ActionCreators'

const DeatilsForm = () => {
  const [first, setFirst] = useState('')
  const [last, setLast] = useState('')
  const [dob, setDob] = useState('')
  const [mobileNo, setMobileNo] = useState('')
  const [img, setImg] = useState('')
  const [dataWorking, setDataWorking] = useState([])

  const handleClear = () => {
    setFirst('')
    setLast('')
    setDob('')
    setMobileNo('')
    setImg('')
  }
  const handleChange = (e) => {
    const { name, value } = e.target
    switch (name) {
      case 'firstName':
        setFirst(value)
        break
      case 'lastName':
        setLast(value)
        break
      case 'dob':
        setDob(value)
        break
      case 'mobileNo':
        setMobileNo(value)
        break
      case 'image':
        setImg(value)
        break
      default:
        return
    }
  }
  const handleSubmit = (e) => {
    e.preventDefault()
    const obj = {
      first,
      last,
      dob,
      mobileNo,
      img
    }
    obj.key = first + last + dob + (dataWorking.length - 1)
    console.log('datatep', ...dataWorking, obj)
    setDataWorking([...dataWorking, obj])
    handleClear()
  }
  return (
    <div>
      <div style={{ display: 'grid' }}>
        <input
          type="text"
          value={first}
          style={{ margin: '3px' }}
          onChange={handleChange}
          name="firstName"
          id="firstName"
          placeholder="Name"
        />
        <input
          type="date"
          name="dob"
          id="dob"
          value={dob}
          style={{ margin: '3px' }}
          onChange={handleChange}
          placeholder="Date of Birth"
        />
        <input
          type="number"
          value={mobileNo}
          style={{ margin: '3px' }}
          onChange={handleChange}
          name="mobileNo"
          id="mobileNo"
          placeholder="Mobile No."
        />
        <input
          type="media"
          value={img}
          style={{ margin: '3px' }}
          onChange={handleChange}
          name="image"
          id="image"
          placeholder="Provide your profile image link"
        />
      </div>
      <div style={{ display: 'flex' }}>
        <button onClick={handleClear}>Reset</button>
        <button onClick={handleSubmit}>Submit</button>
      </div>
    </div>
  )
}

//useSelector
const mapStateToProps = (state) => {
  return {
    reduxData: state.temp
  }
}
//useDispatcher
const mapDispatchToProps = (dispatch) => {
  return {
    fetchOTP: (state) => dispatch(fetchOTP(state))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(DeatilsForm)
