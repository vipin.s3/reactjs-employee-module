import React from 'react'
import { fetchOTP } from './ActionCreators'
import LoginForm from './LoginForm'
import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'

const Login = (props) => {
  console.log('auth', props.auth)
  const history = useHistory()
  const handleLogin = () => {
    history.push('/home')
  }
  return (
    <div>
      <div style={{ display: 'inline-grid' }}>
        <button onClick={() => handleLogin()}>Back To Home</button>
      </div>
      <div>Fill the login form </div>
      <LoginForm />
    </div>
  )
}
//useSelector
const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}
//useDispatcher
const mapDispatchToProps = (dispatch) => {
  return {
    fetchOTP: (state) => dispatch(fetchOTP(state))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login)
