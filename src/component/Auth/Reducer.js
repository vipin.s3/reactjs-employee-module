import { REQUEST_OTP, RECEIVE_OTP, FAILURE_OTP } from 'ActionTypes'

const initialState = {
  isOTPLoading: false,
  isOTPError: false,
  OTPErrorMessage: ''
}
const Auth = (state = initialState, action) => {
  const { type, payload } = action
  switch (type) {
    case REQUEST_OTP:
      return {
        ...state,
        isOTPLoading: true
      }
    case RECEIVE_OTP:
      return {
        ...state,
        isOTPLoading: false,
        isOTPError: false,
        OTP: payload?.data
      }
    case FAILURE_OTP:
      return {
        ...state,
        isOTPLoading: false,
        isOTPError: true,
        OTPErrorMessage: payload?.errorMessage
      }
    default:
      return {
        ...state
      }
  }
}
export default Auth
