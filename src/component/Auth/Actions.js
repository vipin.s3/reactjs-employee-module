import { REQUEST_OTP, RECEIVE_OTP, FAILURE_OTP } from 'ActionTypes'

/**
 * @description Request action for getting home page data
 */
export const requestOTP = () => ({
  type: REQUEST_OTP
})

/**
 * @description Receive action for getting home page data
 */
export const receiveOTP = (payload) => ({
  type: RECEIVE_OTP,
  payload
})

/**
 * @description Failure action for fetching home page data
 */
export const failureOTP = (payload) => ({
  type: FAILURE_OTP,
  payload
})
