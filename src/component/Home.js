import React from 'react'
import Form from './HomePage/DetailsForm'

const Home = () => {
  return (
    <div className="text-center">
      <Form />
    </div>
  )
}

export default Home
