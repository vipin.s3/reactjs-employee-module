/**
 * @description API endpoints
 */

// const { BASE_API_V2 } = process.env.dummy
const { REACT_APP_API_GATEWAY_URL } = process.env

export const DEFAULT_ERROR_MESSAGE = 'Something went wrong.'
const API_V1 = `v1` // For api versions.
export const BASE_API = `${REACT_APP_API_GATEWAY_URL}/api/${API_V1}/`
export const HOME_PAGE_DATA_API = `https://dummy.in/call`
export const LOGIN_API = `https://login.in/call`
