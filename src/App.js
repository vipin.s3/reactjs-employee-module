import React from 'react'
import './App.css'
import AppRouter from './routes'
// import Home from './component/HomePage'

const App = () => {
  return <div className="container">{<AppRouter />}</div>
}

export default App
