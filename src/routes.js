import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import * as constRoutes from 'common/constRoutes'
// import PrivateRoute from './PrivateRoute'
import Home from './component/HomePage'
import Login from './component/Auth'

const AppRouter = () => {
  return (
    <Router>
      <Route path={constRoutes.landing} exact component={Home} />
      <Route path={constRoutes.login} exact component={Login} />
      <Route path={constRoutes.homepage} component={Home} />
    </Router>
  )
}

export default AppRouter
