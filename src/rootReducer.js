import { combineReducers } from 'redux'
import homepage from 'component/HomePage/Reducer'
import auth from 'component/Auth/Reducer'
export default combineReducers({
  home: homepage,
  auth
})
