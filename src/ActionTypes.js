/**
 * @description home page data
 */
export const RECEIVE_HOME = 'RECEIVE_HOME'
export const REQUEST_HOME = 'REQUEST_HOME '
export const FAILURE_HOME = 'FAILURE_HOME'

export const RECEIVE_OTP = 'RECEIVE_OTP'
export const REQUEST_OTP = 'REQUEST_OTP'
export const FAILURE_OTP = 'FAILURE_OTP'
