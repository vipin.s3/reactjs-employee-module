# Frontend Build: React App

We will be :
=>bootstraping our project with [`create-react-app`](https://github.com/facebook/create-react-app).
=>[`materialUI`] for UI we will use [`materialUI`](https://mui.com/)

### 1. Design Principle

    ├── root(Directory)
    │   public (Directory name)
        │   ├── index.html
        │   └── favicons
        |
    │   src (Directory name)
        ├── Assets (Directory name)
        │   ├── xy.png(files)
        │   ├── xyz.svg
        │   └── xyza.jpg
    │   ├── Authentication (Directory name)
        │   ├── Mobile (Directory name)
        │   │   ├── index.js - (Higher Order Component)
        │   │   ├── style.css - (Customized CSS, media-query )
        │   │   ├── Actions.js (redux handling files)
        │   │   ├── ActionsTypes.js (redux handling files)
        │   │   ├── ActionsCreators.js (redux handling files)
        │   │   └── Reducer.js (redux handling files)
        |
        ├── index.js - (Entry Point)
        ├── index.css
        ├── app.js - (Base Component )
        ├── app.css
        ├── store.js(redux store configuration )
        ├── router.js(react routing )
        └── rootreducer.js(combines individual reducers to state)

    ├── .env.dev - (lower env configuration for development)
    ├── .env.qa - (qa env configuration)
    ├── .env.preprod - (preprod env configuration)
    ├── .env.prod - (prod env configuration)
    ├── .babel.config.js - (babel configuration )
    ├── .prettier.rc - (prettier configuration )
    ├── .eslintrc - (es lint configuration) ref : https://www.npmjs.com/package/eslint-config-airbnb
    ├── .stylelintrc - (style lint configuration) ref : https://www.npmjs.com/package/stylelint-config-standard
    ├── jsconfig.json - (Absolute path for app) ref :
    └── package.json - (project dependencies)
    ```

### 2. How to start the development server in project

Step 1 -- Make sure you have installed the extensions required for the project :
EditorConfig
Eslint
gitlens
prettier
docthis

Step 2 -- In the root directory, run `npm install`. This will install the core and the dev dependencies requied for the project

Step 3 -- Run `npm start` to start the development server and open `localhost:3000` in your browser.

# Coding guidelines

Prevalent standards in JavaScript and React

### 1. Branching

Branch name should either be created from featured tasks/stories or it should be a meaningful name.

##### Correct usage

`feature/FEATURE-STORYNO.-description`

##### Incorrect usage

`feature/random-description-timestamp`

---

### 2. Commit message format

```s
    => Every commit message should in active case.
    => Must not be greater than 72 characters.
    => Should summarize the code changes in commit.
    => After a newline, it should have Story number
    => Followed by task description.
    Reference: https://chris.beams.io/posts/git-commit
```

Rules for committing:

```fs
    1. Separate subject from body with a blank line
    2. Limit the subject line to 50 characters
    3. Capitalize the subject line
    4. Do not end the subject line with a period
    5. Use the imperative mood in the subject line
    6. Wrap the body at 72 characters
    7. Use the body to explain what and why vs. How
```

##### Correct usage

`LOGIN-01 : Mobile otp login, Handle no. validation`

##### Incorrect usage

`sso login integrated”, hot fixes`

---

### 3. Avoid making large pull requests so that code review gets easier

### 4. Use semantic HTML elements as per W3C standards(MDN article)

### 5. Use JS Docs Comments

Try to document every function or complex logic by adding comments to it. Comments should
follow JS Docs formatting.

##### Correct usage

For example:

```js
/**
 * @function ssoInitiate
 * @param {String} `src` - api url of the sso
 * @description Loads SSO script and appends to head
 */
const ssoInitiate = (src) => {
 statements...
}
```

##### Incorrect usage

For example:

```js
// Initializes SSO and appends to head
const ssoInitiate = (src) => {
 statements...
}
```

---

### 6. Meaningful Identifiers. Nomenclature(camelCase ) should be relevant

### 7. Prefer ES6 functions strictly over conventional methods :

```fs
        a).Arrow functions,
        b).Proptypes,
        c).Default props
```

### 8. Preferences :

    ```fs
        a).ternary operator if required in nested if-else loops
        b).optional chaining
        c).null coallescing
    ```

### 9. No unnecessary props or arguments

### 10. Always use React’s Prop-Types for type checking

### 11. Use a linter to make your code easier to review(npm run lint)

### 12. Developers code-review to ensure sanity of your feature or hotfix before MR/CR is raised

### 13. If using JEST and enzyme, write your test cases and check coverage before committing your code.

### 14. Objects and arrays destructuring should be preferred

### 15. Unnecessary logs should be removed((console.log/alert))

### 16. Prettier configuration should be constant(Prefer VS code extensions).

    ```sh
        (Not needed if extension is properly configured: tab spaces, single qoutes)
        Run the followng command to prettify the code before raising MR :
        (.prettierrc.js file should be present with required configurations)
        prettier --check "src/**/*.js"
        prettier --write "src/**/*.js"
    ```

### 17. Maintain proper indentation of the blocks

### 18. Use utility functions extensively wherever required

### 19. Follow the camel casing convention in directory and file name

    ```rs
        components(Directory name)
        │ ├── Login (Directory name)
        │ │ ├── index.js - (Higher Order Component)
        │ │ ├── Actions.js -(Redux Managers)
        │ │ ├── ActionsTypes.js -(Redux Managers)
        │ │ ├── ActionsCreators.js -(Redux Managers)
    ```

### 20.Import Sequence

    ```fs
    a).React libraries (including hooks)
    b).Other libraries(redux then router then material UI)
    c).Utilities-Common-Constants (customized hooks)
    d).ActionCreator or Actions
    e).Child Components( then other Higher Order then Lower Order Components )
    f).scss/css files if needed
    ```

### 21.Functional Components(PascalCase) are to be used, with following sequence

    ```fs
        (one blank line between each sections)
            a).props destructuring, const declarations
            b).state declarations
            c).internal functions (Arrow Functions are to be used)
            d).default api calls using useEffect ([] no dependency)
            e).useEffect(dependencies in ascending order)
    ```

### 21.Hooks are always to be used, with dependencies and conditions

### 22.We will preferably use useSelector and useDispatcher if destructuring is not needed, else we will go with mapDispatchtoProps and mapStatetoProps :

    ```js
    import { useDispatch, useSelector } from 'react-redux'
    // import { connect } from 'react-redux'
    import React, { useState, useEffect } from 'react'
    import { actDispatch, cancelDispatch } from './ActionCreators'
    import './test.scss'

    const TempComponent = ({ destructuredProp }) => {

        const dispatch = useDispatch()

        const [isActive, setActive] = useState(false)
        const [count, setCount] = useState(destructuredProp)
        const [pageInfo, setPageInfo] = useState('')

        useEffectOnce(() => {
            dispatch(actDispatch('process'))
            setPageInfo({
                pageName: 'Page',
                category: 'Services'
            })
        },[])

        const getPlans = useSelector((state) => state?.reducerInitial?.details ?? []) // null coallescing

        useEffect(() => {
            if(!destructuredProp) return // conditional call

            if (Array.isArray(getPlans) && getPlans.length) {
                setCount(getPlans?.length) // optional chaining
                setActive(true)
            }
        }, [destructuredProp])

        return ( <><p>{count}</p></>)
    }

    // useSelector alternative
    // const mapStateToProps = (state) => {
    // const {reducerInitial} = state
    //   return {
    //       getPlans: reducerInitial?.details ?? [],
    //       getPlansLoading: reducerInitial?.Loading ? reducerInitial?.Loading : false
    //   }
    // }
    // useDispatcher alternative
    // const mapDispatchToProps = (dispatch) => {
    //     return {
    //         actDispatch: (state) => dispatch(actDispatch(state)),
    //         cancelDispatch: (state) => dispatch(cancelDispatch(state))
    //     }
    // }
    // export default connect(mapStateToProps, mapDispatchToProps)(TempComponent)
    ```

---
